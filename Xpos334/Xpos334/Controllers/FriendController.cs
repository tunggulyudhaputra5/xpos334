﻿using Microsoft.AspNetCore.Mvc;
using Xpos334.Models;

namespace Xpos334.Controllers
{
    public class FriendController : Controller
    {
        private static List<Friend> friends = new List<Friend>()
            {
                new Friend(){Id = 1, Name = "Irvan", address = "Kemayoran"},
                new Friend(){Id = 2, Name = "Tunggul", address = "Cilandak"},
                new Friend(){Id = 3, Name = "Shabrina", address = "Radio Dalam"}
            };
        public IActionResult Index()
        {
            /*List<Friend> friends = new List<Friend>()
            {
                new Friend(){Id = 1, Name = "Irvan", address = "Kemayoran"},
                new Friend(){Id = 2, Name = "Tunggul", address = "Cilandak"},
                new Friend(){Id = 3, Name = "Shabrina", address = "Radio Dalam"}
            };*/

            ViewBag.listFriend = friends;
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Friend friend)
        {
            friends.Add(friend);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int Id)
        {
            //Friend friend = friends[Id];
            Friend friend = friends.Find(a => a.Id == Id)!;
            return View(friend);
        }
        [HttpPost]
        public IActionResult Edit(Friend data)
        {
            Friend friend = friends.Find(a => data.Id == data.Id)!;
            int index = friends.IndexOf(friend);

            if (index > -1)
            {
                friends[index].Name = data.Name;
                friends[index].address = data.address;
            }

            return RedirectToAction("Index");

        }
        public IActionResult Detail(int Id)
        {
            Friend friend = friends.Find(a => a.Id == Id)!;
            return View(friend);
        }
        public IActionResult Delete(int Id)
        {
            Friend friend = friends.Find(a => a.Id == Id)!;
            return View(friend);
        }
        [HttpPost]
        public IActionResult Delete(Friend data)
        {
            Friend friend = friends.Find(a => a.Id == data.Id)!;
            friends.Remove(friend);
            return RedirectToAction("Index");
        }
    }
}

