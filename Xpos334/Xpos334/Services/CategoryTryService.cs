﻿using AutoMapper;
using System.Collections.Generic;
using System.Data;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace Xpos334.Services
{
    public class CategoryTryService
    {
        private readonly XPOS_334Context db;
        VMResponse respon = new VMResponse();
        int IdUser = 1;

        public CategoryTryService(XPOS_334Context _db)
        {
            db = _db;
        }
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblCategory, VMTblCategory>();
                cfg.CreateMap<VMTblCategory, TblCategory > ();
            });

            IMapper mapper = config.CreateMapper();
            return mapper;
        }

        public List<VMTblCategory> GetAllData()
        {
            List<TblCategory> dataModel = db.TblCategories.Where(a => a.IsDelete == false).ToList();

            List <VMTblCategory> dataView = GetMapper().Map<List<VMTblCategory>>(dataModel);

            return dataView;
        }

        public VMResponse Create(VMTblCategory dataView)
        {
            TblCategory dataModel = GetMapper().Map<TblCategory>(dataView);
            dataModel.IsDelete = false;
            dataModel.CreateBy = IdUser;
            dataModel.CreateDate = DateTime.Now;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                respon.Message = "Data Success saved";
                respon.Entity = dataModel;
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved" + ex.Message;
                respon.Entity = dataView;
            }

            return respon;
        }

        public VMTblCategory GetById(int id)
        {
            TblCategory dataModel = db.TblCategories.Find(id);
            //TblCategory dataModel = db.TblCategories.Where(a => a.Id == id).FirstOrDefault();
            VMTblCategory dataView = GetMapper().Map<VMTblCategory>(dataModel);
            return dataView;
        }
        public VMResponse Edit(VMTblCategory dataView)
        {
            TblCategory dataModel = db.TblCategories.Find(dataView.Id);
            dataModel.NameCategory = dataView.NameCategory;
            dataModel.Description = dataView.Description;
            dataModel.UpdateBy = IdUser;
            dataModel.UpdateDate = DateTime.Now;


            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success save";
                respon.Entity =GetMapper().Map<VMTblCategory>(dataModel);
            }
            catch(Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : "+ ex.Message;
                respon.Entity = GetMapper().Map<VMTblCategory>(dataModel);

            }
            return respon;

        }

        public VMResponse Delete(VMTblCategory dataView)
        {
            TblCategory dataModel = db.TblCategories.Find(dataView.Id);
            dataModel.IsDelete = true;
            dataModel.UpdateBy = IdUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success delete";
                respon.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
                respon.Entity = GetMapper().Map<VMTblCategory>(dataModel);

            }
            return respon;
        }
    }
}

