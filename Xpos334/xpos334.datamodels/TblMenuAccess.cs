﻿using System;
using System.Collections.Generic;

namespace xpos334.datamodels
{
    public partial class TblMenuAccess
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int? MenuId { get; set; }
        public bool? IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
