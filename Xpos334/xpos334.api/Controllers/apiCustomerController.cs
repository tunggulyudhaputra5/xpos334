﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCustomerController(XPOS_334Context _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data = (from v in db.TblCustomers
                                        join c in db.TblRoles on v.IdRole equals c.Id
                                        where v.IsDelete == false
                                        select new VMTblCustomer
                                        {
                                            Id = v.Id,
                                            NameCustomer = v.NameCustomer,
                                            Email = v.Email,
                                            Password = v.Password,
                                            Address = v.Address,
                                            Phone = v.Phone,

                                            IdRole = v.IdRole,
                                            RoleName = c.RoleName,

                                            IsDelete = v.IsDelete,
                                            CreateDate = v.CreateDate,
                                        }).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblCustomer GetDataById(int id)
        {
            VMTblCustomer data = (from c in db.TblCustomers
                                  join r in db.TblRoles on c.IdRole equals r.Id
                                  where c.IsDelete == false && c.Id == id
                                  select new VMTblCustomer
                                  {
                                      Id = c.Id,
                                      NameCustomer = c.NameCustomer,
                                      Email = c.Email,
                                      Password = c.Password,
                                      Address = c.Address,
                                      Phone = c.Phone,

                                      IdRole = c.IdRole,
                                      RoleName = r.RoleName,

                                      IsDelete = c.IsDelete
                                  }).FirstOrDefault()!;
            return data;
        }
        [HttpGet("GetDataByIdRole/{id}")]
        public List<VMTblCustomer> GetDataByIdCategory(int id)
        {
            List<VMTblCustomer> data = (from c in db.TblCustomers
                                        join r in db.TblRoles on c.IdRole equals r.Id
                                        where c.IsDelete == false && c.IdRole == id
                                        select new VMTblCustomer
                                        {
                                            Id = c.Id,
                                            NameCustomer = c.NameCustomer,
                                            Email = c.Email,
                                            Password = c.Password,
                                            Address = c.Address,
                                            Phone = c.Phone,

                                            IdRole = c.IdRole,
                                            RoleName = r.RoleName,
                                        }).ToList();
            return data;
        }
        [HttpGet("CheckByName/{email}/{id}")]
        public bool CheckName(string email, int id)
        {
            TblCustomer data = new TblCustomer();
            if (id == 0)
            {
                data = db.TblCustomers.Where(a => a.Email == email && a.IsDelete == false).FirstOrDefault()!;
            }
            else
            {
                data = db.TblCustomers.Where(a => a.Email == email && a.IsDelete == false
                && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            return false;
        }


        [HttpPost("Save")]
        public VMResponse Save(TblCustomer data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCustomer data)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.NameCustomer = data.NameCustomer;
                dt.Email = data.Email;
                dt.Password = data.Password;
                dt.Address = data.Address;
                dt.Phone = data.Phone;
                dt.IdRole = data.IdRole;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}")]

        public VMResponse Delete(int id)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }
    }
}