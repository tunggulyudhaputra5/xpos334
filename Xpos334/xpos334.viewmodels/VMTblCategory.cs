﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xpos334.viewmodels
{
    public class VMTblCategory
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Isi semua dan tidak boleh kosong")]
        [StringLength(10)]//jika tidak ada error message maka akan default sistem
        [MinLength(3, ErrorMessage ="isi min 3 karakter")]
        public string NameCategory { get; set; } = null!;
        [Required(ErrorMessage = "Isi semua dan tidak boleh kosong")]
        public string? Description { get; set; }
        public bool? IsDelete { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
